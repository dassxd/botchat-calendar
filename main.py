import json
import pytz
from datetime import datetime, timedelta

from telegram import Update
from telegram.ext import Application, CallbackQueryHandler, CommandHandler, ContextTypes, ConversationHandler, MessageHandler, filters

from google_calendar import GoogleCalendarManager

NAME, START_TIME, DURATION, EMAILS = range(4)
CREDENDIALS = None

calendar_manager = GoogleCalendarManager()


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text('*Hola soy un Bot Task :)* \n ¿Que deseas hacer?',
                                    parse_mode='Markdown',
                                    reply_markup={'inline_keyboard': [
                                        [{'text': 'Crear', 'callback_data': 'create'},
                                         {'text': 'Listar', 'callback_data': 'list'}]
                                    ]})


async def list(update: Update, context: ContextTypes.DEFAULT_TYPE):
    events = calendar_manager.list_upcoming_events()
    if not events:
        await update.callback_query.message.reply_text('No hay eventos disponibles.')
    else:
        await update.callback_query.message.reply_text(f'📅 *Hay {len(events)} eventos*' if len(events) > 1 else '*Hay 1 evento:*', parse_mode='Markdown')
        for event in events:
            start_time = datetime.strptime(
                event['start'].get('dateTime'), '%Y-%m-%dT%H:%M:%S%z')
            start_time = start_time.astimezone(pytz.timezone("America/Bogota"))
            end_time = datetime.strptime(
                event['end'].get('dateTime'), '%Y-%m-%dT%H:%M:%S%z')
            end_time = end_time.astimezone(pytz.timezone("America/Bogota"))
            respose = (f'*{event["summary"]}*\n'
                       f'Fecha de inicio: {start_time.strftime("%Y-%m-%d %H:%M")}\n'
                       f'Fecha final: {end_time.strftime("%Y-%m-%d %H:%M")}\n\n')
            # boton de accion para eliminar evento por id
            button = [[{'text': 'Eliminar', 'callback_data': f'delete_{event["id"]}'},
                       {'text': 'Actualizar', 'callback_data': f'update_{event["id"]}'}]]
            await update.callback_query.message.reply_text(respose, reply_markup={'inline_keyboard': button}, parse_mode='Markdown')
        await update.callback_query.message.reply_text('/start para volver al menú principal.')


async def get_event(update: Update, context: ContextTypes.DEFAULT_TYPE):
    data = update.callback_query.data.split('_')
    if len(data) == 3:
        action, id, argument = data
    elif len(data) == 2:
        action, id, argument = data + [None]
    else:
        action, id, argument = data + [None, None]
    # print('🙉', action, id, argument)
    if id:
        event = calendar_manager.get_event(id)
        context.user_data['detail_event'] = event
        if event is None:
            return await update.callback_query.message.reply_text('Evento no encontrado.'
                                                                  '\n\n /start para volver al menú principal.')

    if action == 'create':
        context.user_data['mode_update'] = False
        await update.callback_query.message.reply_text('*Creando un nuevo evento!*\nUtiliza /cancel para cancelar el proceso de creación.', parse_mode='Markdown')
        await update.callback_query.message.reply_text('Por favor, proporciona el nombre del evento')
        return NAME
    elif action == 'list':
        await list(update, context)
    elif action == 'delete':
        if argument == 'yes':
            calendar_manager.delete_event(id)
            await update.callback_query.message.reply_text('🗑 Evento eliminado con éxito.'
                                                           '\n\n /start para volver al menú principal.')
        elif argument == 'no':
            await update.callback_query.message.reply_text('🗑 Proceso de eliminación cancelado.'
                                                           '\n\n /start para volver al menú principal.')
        else:
            await update.callback_query.message.reply_text(f'🗑 ¿Estas seguro de eliminar el evento *{event["summary"]}*?',
                                                           reply_markup={'inline_keyboard': [
                                                               [{'text': 'Si', 'callback_data': f'delete_{id}_yes'},
                                                                {'text': 'No', 'callback_data': f'delete_{id}_no'}]]},
                                                           parse_mode='Markdown')
    elif action == 'update':
        if argument == 'yes':
            _message = await update.callback_query.message.reply_text('🔄 Actualizando evento...')
            _summary = context.user_data['detail_event_update'].get(
                'summary', None)
            _start_time = context.user_data['detail_event_update'].get(
                'start_time', None)
            if _start_time:
                _duration = datetime.strptime(context.user_data['detail_event']['end'].get('dateTime'), '%Y-%m-%dT%H:%M:%S%z').astimezone(pytz.timezone("America/Bogota")).replace(
                    tzinfo=None) - datetime.strptime(context.user_data['detail_event']['start'].get('dateTime'), '%Y-%m-%dT%H:%M:%S%z').astimezone(pytz.timezone("America/Bogota")).replace(tzinfo=None)
                print('😃', _duration, type(_duration))
                _end_time = _start_time + _duration
            else:
                _end_time = context.user_data['detail_event_update'].get(
                    'end_time', None)
            event = calendar_manager.update_event(
                id, summary=_summary, start_time=_start_time, end_time=_end_time)
            context.user_data['mode_update'] = False
            await _message.edit_text('🔄 Evento actualizado con éxito.'
                                     '\n\n /start para volver al menú principal.')
        elif argument == 'no':
            await update.callback_query.message.reply_text('🔄 Proceso de actualización cancelado.'
                                                           '\n\n /start para volver al menú principal.')
            context.user_data['mode_update'] = False
        elif argument == 'name':
            await update.callback_query.message.reply_text('🔄 Por favor, proporciona el nuevo nombre del evento.')
            context.user_data['mode_update'] = True
            return NAME
        elif argument == 'duration':
            await update.callback_query.message.reply_text('🔄 Por favor, proporciona la nueva duración del evento (en minutos).')
            context.user_data['mode_update'] = True
            return DURATION
        elif argument == 'date':
            await update.callback_query.message.reply_text('🔄 Por favor, proporciona la nueva fecha de inicio del evento (formato: AAAA-MM-DD HH:MM).')
            context.user_data['mode_update'] = True
            return START_TIME
        else:
            context.user_data['detail_event_update'] = {}
            await update.callback_query.message.reply_text(f'🔄 ¿Que deseas actualizar del evento *{event["summary"]}*?',
                                                           reply_markup={'inline_keyboard': [
                                                               [{'text': 'Nombre', 'callback_data': f'update_{id}_name'},
                                                                {'text': 'Duración', 'callback_data': f'update_{id}_duration'}],
                                                               [{'text': 'Fecha de inicio', 'callback_data': f'update_{id}_date'}]]},
                                                           parse_mode='Markdown')


async def get_name(update, context):
    if update.message.text == '/cancel':
        return await cancel(update, context)
    name = update.message.text
    if "/" in name:
        await update.message.reply_text(
            'El nombre del evento no puede contener "/". Por favor, inténtalo de nuevo.')
        return NAME
    if context.user_data.get('mode_update', False):
        context.user_data['detail_event_update']['summary'] = name
        await update.message.reply_text('🔄 ¿Estas seguro de actualizar el nombre del evento?',
                                        reply_markup={'inline_keyboard': [
                                            [{'text': 'Si', 'callback_data': f'update_{context.user_data["detail_event"]["id"]}_yes'},
                                             {'text': 'No', 'callback_data': f'update_{context.user_data["detail_event"]["id"]}_no'}]
                                        ]})
        return ConversationHandler.END
    context.user_data['name'] = name
    await update.message.reply_text(
        'Por favor, proporciona la fecha de inicio (formato: AAAA-MM-DD HH:MM)')
    return START_TIME


async def get_start_datetime(update, context):
    if update.message.text == '/cancel':
        return await cancel(update, context)
    try:
        time = datetime.strptime(
            update.message.text, '%Y-%m-%d %H:%M')
    except ValueError:
        await update.message.reply_text(
            'El formato de fecha no es válido. Por favor, inténtalo de nuevo.')
        return START_TIME
    if time < datetime.now():
        await update.message.reply_text(
            'La fecha de inicio no puede ser anterior a la fecha actual. Por favor, inténtalo de nuevo.')
        return START_TIME
    if context.user_data.get('mode_update', False):
        context.user_data['detail_event_update']['start_time'] = time
        await update.message.reply_text('🔄 ¿Estas seguro de actualizar la fecha de inicio del evento?',
                                        reply_markup={'inline_keyboard': [
                                            [{'text': 'Si', 'callback_data': f'update_{context.user_data["detail_event"]["id"]}_yes'},
                                             {'text': 'No', 'callback_data': f'update_{context.user_data["detail_event"]["id"]}_no'}]
                                        ]})
        return ConversationHandler.END
    context.user_data['start_time'] = time
    await update.message.reply_text(
        'Ahora, por favor, proporciona la duración del evento (en minutos)')
    return DURATION


async def get_end_datetime(update, context):
    if update.message.text == '/cancel':
        return await cancel(update, context)
    try:
        duration = int(update.message.text)
    except ValueError:
        await update.message.reply_text(
            'El formato de duración no es válido. Por favor, inténtalo de nuevo.')
        return DURATION
    if duration <= 0:
        await update.message.reply_text(
            'La duración debe ser mayor que 0. Por favor, inténtalo de nuevo.')
        return DURATION
    if context.user_data.get('mode_update', False):
        _time = datetime.strptime(context.user_data['detail_event']['start'].get(
            'dateTime'), '%Y-%m-%dT%H:%M:%S%z')
        context.user_data['detail_event_update']['end_time'] = _time + \
            timedelta(minutes=duration)
        await update.message.reply_text('🔄 ¿Estas seguro de actualizar la duración del evento?',
                                        reply_markup={'inline_keyboard': [
                                            [{'text': 'Si', 'callback_data': f'update_{context.user_data["detail_event"]["id"]}_yes'},
                                             {'text': 'No', 'callback_data': f'update_{context.user_data["detail_event"]["id"]}_no'}]
                                        ]})
        return ConversationHandler.END
    context.user_data['duration'] = duration
    context.user_data['end_time'] = context.user_data['start_time'] + \
        timedelta(minutes=duration)
    await update.message.reply_text(
        'Por último, proporciona los correos electrónicos de los asistentes (separados por comas)')
    return EMAILS


async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text('Proceso de creación cancelado. ¡Hasta luego!'
                                    '\n\n /start para volver al menú principal.')
    return ConversationHandler.END


async def get_emails(update, context):
    if update.message.text == '/cancel':
        return await cancel(update, context)
    emails = update.message.text.replace(' ', '').split(',')
    for email in emails:
        if '@' not in email or '.' not in email:
            await update.message.reply_text(
                'Uno de los correos electrónicos no es válido. Por favor, inténtalo de nuevo.')
            return EMAILS
    context.user_data['emails'] = emails
    await update.message.reply_text('¡Información recopilada con éxito!\n'
                                    f'Nombre: {context.user_data["name"]}\n'
                                    f'Fecha de inicio: {context.user_data["start_time"].strftime("%Y-%m-%d %H:%M")}\n'
                                    f'Fecha final: {context.user_data["end_time"].strftime("%Y-%m-%d %H:%M")}\n'
                                    f'Duración: {context.user_data["duration"]} minutos\n'
                                    f'Correos electrónicos: {", ".join(context.user_data["emails"])}')
    _message = await update.message.reply_text('Creando evento...')
    event = None
    event = calendar_manager.create_event(summary=context.user_data['name'],
                                          start_time=context.user_data['start_time'],
                                          end_time=context.user_data['end_time'],
                                          attendees=context.user_data['emails'],
                                          timezone="America/Bogota")
    if not event:
        await _message.edit_text('Ha ocurrido un error al crear el evento.'
                                 '\n\n /start para volver al menú principal.')
    else:
        await _message.edit_text('Evento creado con éxito.'
                                 '\n\n /start para volver al menú principal.')
    return ConversationHandler.END


def response(text: str, context: ContextTypes, Update: Update):
    pro_text = text.lower()
    print('➡', pro_text)
    if 'hola' in pro_text:
        return 'Hola, buen día'
    elif 'adios' in pro_text:
        return 'Bye bye.'
    else:
        return 'No te entiendo :('


async def message(update: Update, context: ContextTypes):
    mss_type = update.message.chat.type
    text = update.message.text
    if mss_type == 'group':
        if text.startswith(CREDENDIALS['user_name'], ' '):
            new_text = text.replace(CREDENDIALS['user_name'], ' ')
            res = response(new_text, context, update)
        else:
            return
    else:
        res = response(text, context, update)
    await update.message.reply_text(res)


async def error(update: Update, context: ContextTypes):
    print(f'💥 Update {update} caused error {context.error}')
    if update.message is not None:
        await update.message.reply_text('🤔 Ha ocurrido un error, por favor intentalo de nuevo.')
    else:
        await update.callback_query.message.reply_text('🤔 Ha ocurrido un error, por favor intentalo de nuevo.')


def run():
    with open('telegram_credentials.json', 'r') as file:
        CREDENDIALS = json.load(file)
    app_bot = Application.builder().token(CREDENDIALS['token']).build()
    app_bot.add_handler(CommandHandler('start', start))

    conv_handler = ConversationHandler(
        entry_points=[CallbackQueryHandler(get_event)],
        states={
            NAME: [MessageHandler(filters.TEXT, get_name)],
            START_TIME: [MessageHandler(filters.TEXT, get_start_datetime)],
            DURATION: [MessageHandler(filters.TEXT, get_end_datetime)],
            EMAILS: [MessageHandler(filters.TEXT, get_emails)],
        },
        fallbacks=[CommandHandler('cancel', cancel)]
    )
    app_bot.add_handler(conv_handler)
    app_bot.add_handler(MessageHandler(filters.TEXT, message))
    app_bot.add_error_handler(error)
    print('Bot Iniciado 🤖')
    app_bot.run_polling(poll_interval=1, timeout=10,
                        allowed_updates=Update.ALL_TYPES)


if __name__ == '__main__':
    run()
