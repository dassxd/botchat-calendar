# Chatbot Calendar

El bot hace uso de la API de Google Calendar y de la API de Telegram.

Chatbot para la gestión de eventos en un calendario de Google Calendar, el cual permite crear, eliminar y listar eventos. Se puede configurar para nuestra cuenta de Google Calendar a traves de nuestro propio bot.

Al iniciar el bot se crea un token de sesión que se almacena en un fichero para que no sea necesario volver a iniciar sesión cada vez que se inicie el bot.

Se hizo uso de Botones Inline para la interacción con el usuario y de Conversations para la gestión de la formulario de creación y edición de eventos.

## Configuración

**telegram_credentials:** Información del bot creado en Telegram

**google_credentials:** Información de la cuenta de Google Calendar (es necesario crear un proyecto en Google Cloud Platform y activar la API de Google Calendar)


## Instalación

```bash
python -m venv .venv
# Si estas en windows
source .venv/Scripts/activate
# Si estas en Linux
source .venv/bin/activate
pip install -r requirements.txt
```

`Es necesario python 3.8 en adelante`

## Uso

Para la puesta en marcha del proyecto se debe ejecutar el siguiente comando:

```bash
python main.py
```
