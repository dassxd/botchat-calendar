import os.path
import datetime as dt

from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

SCOPES = ["https://www.googleapis.com/auth/calendar"]


class GoogleCalendarManager:
    def __init__(self):
        self.service = self._authenticate()

    def _authenticate(self):
        creds = None

        if os.path.exists("token_session.json"):
            creds = Credentials.from_authorized_user_file(
                "token_session.json", SCOPES)

        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    "google_credentials.json", SCOPES)
                creds = flow.run_local_server(port=0)

            # Guardar las credenciales para la proxima vez
            with open("token_session.json", "w") as token:
                token.write(creds.to_json())

        return build("calendar", "v3", credentials=creds)

    def list_upcoming_events(self, max_results=10):
        now = dt.datetime.utcnow().isoformat() + "Z"
        tomorrow = (dt.datetime.now() +
                    dt.timedelta(days=7)).replace(hour=23, minute=59, second=0, microsecond=0).isoformat() + "Z"
        events_result = self.service.events().list(
            calendarId='primary', timeMin=now, timeMax=tomorrow,
            maxResults=max_results, singleEvents=True,
            orderBy='startTime'
        ).execute()
        events = events_result.get('items', [])

        if events:
            for event in events:
                start = event['start'].get(
                    'dateTime', event['start'].get('date'))
        return events

    def create_event(self, summary, start_time, end_time, timezone, attendees=None):
        start_time = start_time.isoformat()
        end_time = end_time.isoformat()
        event = {
            'summary': summary,
            'start': {
                'dateTime': start_time,
                'timeZone': timezone,
            },
            'end': {
                'dateTime': end_time,
                'timeZone': timezone,
            }
        }
        if attendees:
            event["attendees"] = [{"email": email} for email in attendees]

        try:
            event = self.service.events().insert(calendarId="primary", body=event).execute()
            return event
        except HttpError as error:
            print(f"A ocurrido un error: {error}")

    def update_event(self, event_id, summary=None, start_time=None, end_time=None):
        event = self.get_event(event_id)

        if summary:
            event['summary'] = summary
        if start_time:
            event['start']['dateTime'] = start_time.strftime(
                '%Y-%m-%dT%H:%M:%S')
        if end_time:
            event['end']['dateTime'] = end_time.strftime('%Y-%m-%dT%H:%M:%S')

        updated_event = self.service.events().update(
            calendarId='primary', eventId=event_id, body=event).execute()
        return updated_event

    def delete_event(self, event_id):
        self.service.events().delete(
            calendarId='primary', eventId=event_id).execute()
        return True
    
    def get_event(self, event_id):
        try:
            event = self.service.events().get(
                calendarId='primary', eventId=event_id).execute()
        except HttpError as error:
            print(f"A ocurrido un error: {error}")
            event = None
        return event